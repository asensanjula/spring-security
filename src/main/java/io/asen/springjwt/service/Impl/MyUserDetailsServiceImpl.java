package io.asen.springjwt.service.Impl;

import io.asen.springjwt.common.enums.Role;
import io.asen.springjwt.dao.entity.UserEntity;
import io.asen.springjwt.dao.repository.UserRepository;
import io.asen.springjwt.dto.SecurityUser;
import io.asen.springjwt.dto.User;
import io.asen.springjwt.service.MyUserDetailsService;

import io.asen.springjwt.service.helper.UserServiceHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

/** The user function related service layer implementation classes*/
@Service
@Transactional
public class MyUserDetailsServiceImpl implements MyUserDetailsService
{
    @Autowired
    private UserServiceHelper userServiceHelper;

    /** User repository to access user table */
    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException
    {
        Optional<UserEntity> optional = userRepository.getUserEntityByUsername( username );
        if( optional.isPresent() )
        {
            UserEntity entity = optional.get();
            SecurityUser securityUser = new SecurityUser(entity.getUsername(), entity.getPassword(), entity.getUserId(), entity.getId(), entity.getRole());
            return securityUser;
        }

        return null;
    }

    @Override
    public User registerUser( User user )
    {
        UserEntity userModel = userServiceHelper.convertToUserEntity(user);
        userModel.setRole( Role.USER );

        userRepository.save( userModel );

        return user;

    }
}
