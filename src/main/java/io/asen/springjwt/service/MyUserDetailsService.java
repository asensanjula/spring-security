package io.asen.springjwt.service;


import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import io.asen.springjwt.dto.*;

import java.util.ArrayList;

@Service
public interface MyUserDetailsService extends UserDetailsService
{
    User registerUser( User user );
}
