package io.asen.springjwt.service.helper;

import io.asen.springjwt.dao.entity.UserEntity;
import io.asen.springjwt.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserServiceHelper
{
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /** This method convert user request to user entity */
    public UserEntity convertToUserEntity( User user )
    {
        UserEntity userModel = new UserEntity();
        userModel.setUsername( user.getEmailAddress() );
        userModel.setPassword( bCryptPasswordEncoder.encode( user.getPassword() ) );
        userModel.setUserId( UUID.randomUUID().toString() );

        return userModel;

    }
}
