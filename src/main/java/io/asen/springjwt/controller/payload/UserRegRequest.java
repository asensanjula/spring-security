package io.asen.springjwt.controller.payload;

import lombok.Data;

@Data
public class UserRegRequest extends Request
{
    /** The property emailAddress is the generated email address for user */
    private String emailAddress;

    /** The property password is the generated password for user */
    private String password;

}
