package io.asen.springjwt.controller.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegResponse extends Response
{
    /** The property userId is the generated user id of the user */
    private String userId;
}
