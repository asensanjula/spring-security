package io.asen.springjwt.controller;

import io.asen.springjwt.controller.helper.UserControllerHelper;
import io.asen.springjwt.controller.payload.UserRegRequest;
import io.asen.springjwt.controller.payload.UserRegResponse;
import io.asen.springjwt.dto.User;

import io.asen.springjwt.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserController
{
    @Autowired
    private UserControllerHelper helper;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @PostMapping("/addUser")
    public UserRegResponse registerUser( @RequestBody UserRegRequest request)
    {
        User user = helper.convert(request);
        myUserDetailsService.registerUser( user );

        UserRegResponse response = new UserRegResponse();

        return response;

    }

    @GetMapping("/firstPage")
    public String getFirstPage()
    {
        return ("<h1>First Page</h1>");
    }
}
