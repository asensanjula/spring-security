package io.asen.springjwt.controller;

import io.asen.springjwt.security.LoginDetail;
import io.asen.springjwt.security.LoginResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController
{
    @PostMapping(value = "/auth")
    public ResponseEntity<?> createAuthenticationToken( @RequestBody LoginDetail loginDetail) throws Exception {

        return ResponseEntity.ok(new LoginResponse( "token" ) );
    }

}
