package io.asen.springjwt.controller.helper;


import io.asen.springjwt.controller.payload.UserRegRequest;
import io.asen.springjwt.dto.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
/** The UserControllerHelper is the helper class for user controller */
public class UserControllerHelper
{
    @Autowired
    private ModelMapper modelMapper;

    public User convert( UserRegRequest request) {

        User user = User.builder().build();
        modelMapper.map(request, user);
        return user;
    }
}
