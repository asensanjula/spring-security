package io.asen.springjwt.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * The class as the base class for Dtos
 */
public class BaseDto
{

    @Override
    public String toString()
    {

        return ToStringBuilder.reflectionToString( this, ToStringStyle.JSON_STYLE );
    }
}

