package io.asen.springjwt.dto;

import io.asen.springjwt.common.enums.Role;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Arrays;

@Getter
@Setter
public class SecurityUser extends User
{
    private String userId;

    private Long id;

    private String sessionId;

    private Role role;

    public SecurityUser(String username, String password, String userId, Long id, Role role) {

        super(username, password, Arrays.asList(new SimpleGrantedAuthority(role.name())));
        this.userId = userId;
        this.id = id;
        this.role = role;
    }
}
