package io.asen.springjwt.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class User extends BaseDto
{
    /** The property emailAddress is the generated email address for user */
    private String emailAddress;

    /** The property password is the generated password for user */
    private String password;
}
