package io.asen.springjwt.common.enums;

public enum Role {

    USER, ADMIN
}
