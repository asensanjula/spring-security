package io.asen.springjwt.security;

import io.asen.springjwt.controller.payload.Response;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponse extends Response
{
    private String jwt;

    public LoginResponse( String jwt )
    {
        this.jwt = jwt;
    }
}
