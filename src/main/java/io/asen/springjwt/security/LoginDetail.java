// *************************************************************************************************
//
// PROJECT : ugc-std-reg
// PRODUCT : UGC Student Registration
// CLASS : LoginDetail
// PURPOSE : UGC Student Registration
// ************************************************************************************************
//
// Copyright(C) 2019 SprintLabs (Pvt) Ltd.
// All rights reserved.
//
// THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
// SprintLabs (Pvt) Ltd.
//
// This copy of the Source Code is intended for SprintLabs (Pvt) Ltd 's internal use only
// and is
// intended for view by persons duly authorized by the management of SprintLabs (Pvt) Ltd. 
//No part of this file may be reproduced or distributed in any form or by any
// means without the written approval of the Management of SprintLabs (Pvt) Ltd.
//
// *************************************************************************************************

package io.asen.springjwt.security;
import io.asen.springjwt.controller.payload.Request;
import lombok.Getter;
import lombok.Setter;

/** The class used to map the login information sent by user */
@Getter
@Setter
public class LoginDetail extends Request
{
    /** The property username is the username*/
    private String username;

    /** The property password is the password*/
    private String password;
}
