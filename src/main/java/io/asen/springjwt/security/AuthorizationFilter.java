package io.asen.springjwt.security;

import io.asen.springjwt.common.enums.Role;
import io.asen.springjwt.dto.SecurityUser;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

@Slf4j
public class AuthorizationFilter extends BasicAuthenticationFilter
{

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationFilter.class);

    public AuthorizationFilter( AuthenticationManager authenticationManager )
    {
        super( authenticationManager );
    }

    @Override
    protected void doFilterInternal( HttpServletRequest request, HttpServletResponse response, FilterChain filterChain ) throws IOException, ServletException
    {
        Authentication authentication = getAuthentication(request);
        if (authentication == null) {

            filterChain.doFilter(request, response);
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }

    /**
     * The method will retrieve the user information from the JWT toked if a valid and unaltered
     * @param request Servlet
     * @return the username password authentication token
     */
    private Authentication getAuthentication(HttpServletRequest request) {

        String token = request.getHeader(SecurityConstants.TOKEN_HEADER);
        if ( StringUtils.isNotEmpty(token) && token.startsWith(SecurityConstants.TOKEN_PREFIX)) {

            try {

                byte[] signingKey = SecurityConstants.JWT_SECRET.getBytes();
                Jws<Claims> parsedToken = Jwts.parser()
                                              .setSigningKey(signingKey)
                                              .parseClaimsJws(token.replace("Bearer ", ""));

                String username = parsedToken
                                          .getBody()
                                          .getSubject();

                List<String> roles = (List<String>)parsedToken.getBody().get("rol");
                Role role = Role.valueOf(roles.get(0));
                List<SimpleGrantedAuthority> authorities = roles.stream()
                                                                .map(authority -> new SimpleGrantedAuthority((String) authority))
                                                                .collect( Collectors.toList());

                String userId = (String)parsedToken.getBody()
                                                   .get("userId");
//                String sessionId = (String)parsedToken.getBody()
//                                                      .get("sessionId");

                SecurityUser securityUser = new SecurityUser(username, "", userId, null, role);
//                securityUser.setSessionId(sessionId);
//                log.info("Current User Id  {}", userId);
                if (StringUtils.isNotEmpty(username)) {
                    return new UsernamePasswordAuthenticationToken(securityUser, null, authorities);
                }
            } catch ( ExpiredJwtException exception) {

                LOGGER.warn("Request to parse expired JWT : {} failed : {}", token, exception.getMessage());
                throw exception;
            } catch ( UnsupportedJwtException exception) {

                LOGGER.warn("Request to parse unsupported JWT : {} failed : {}", token, exception.getMessage());
                throw exception;
            } catch (MalformedJwtException exception) {

                LOGGER.warn("Request to parse invalid JWT : {} failed : {}", token, exception.getMessage());
                throw exception;
            } catch ( SignatureException exception) {

                LOGGER.warn("Request to parse JWT with invalid signature : {} failed : {}", token, exception.getMessage());
                throw exception;
            } catch (IllegalArgumentException exception) {

                LOGGER.warn("Request to parse empty or null JWT : {} failed : {}", token, exception.getMessage());
                throw exception;
            }
        }
        return null;
    }
}
