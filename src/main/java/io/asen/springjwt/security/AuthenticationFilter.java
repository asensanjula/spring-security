package io.asen.springjwt.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.asen.springjwt.dto.SecurityUser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter
{
    /**
     * The authentication manager
     */
    private final AuthenticationManager authenticationManager;


    public AuthenticationFilter( AuthenticationManager authenticationManager )
    {
        this.authenticationManager = authenticationManager;
        setFilterProcessesUrl( SecurityConstants.AUTH_LOGIN_URL );
    }


    @Override
    public Authentication attemptAuthentication( HttpServletRequest request, HttpServletResponse response ) throws AuthenticationException
    {
        ObjectMapper mapper = new ObjectMapper();
        LoginDetail loginDetail = null;
        try
        {
            loginDetail = mapper.readValue(request.getReader(), LoginDetail.class);
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken( loginDetail.getUsername(), loginDetail.getPassword() , new ArrayList<>(  ) );
        return authenticationManager.authenticate( authenticationToken );
    }


    @Override
    protected void successfulAuthentication( HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult ) throws IOException, ServletException
    {
        super.successfulAuthentication( request, response, chain, authResult );
        SecurityUser user = ((SecurityUser) authResult.getPrincipal());
//        log.info("SecurityUser {}", user);

        List<String> roles = user.getAuthorities()
                                 .stream()
                                 .map( GrantedAuthority::getAuthority)
                                 .collect( Collectors.toList());

        byte[] signingKey = SecurityConstants.JWT_SECRET.getBytes();
//        log.debug("generating JWT");
        String token = Jwts.builder()
                           .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS256)
                           .setHeaderParam("typ", SecurityConstants.TOKEN_TYPE)
//                           .setIssuer(SecurityConstants.TOKEN_ISSUER)
//                           .setAudience(SecurityConstants.TOKEN_AUDIENCE)
                           .setSubject(user.getUsername())
                           .setExpiration(new Date(System.currentTimeMillis() + 864000000))
                           .claim("rol", roles)
                           .claim("userId", user.getUserId())
                           .compact();
        response.addHeader(SecurityConstants.TOKEN_HEADER, SecurityConstants.TOKEN_PREFIX + token);

        System.out.println("return JWT");
    }
}
