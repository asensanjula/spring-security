package io.asen.springjwt.dao.repository;

import io.asen.springjwt.dao.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity , Long>
{
    Optional<UserEntity> getUserEntityByUsername( String username);
}
