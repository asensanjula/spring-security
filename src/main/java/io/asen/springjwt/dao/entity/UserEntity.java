package io.asen.springjwt.dao.entity;

import io.asen.springjwt.common.enums.Role;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "um_user")
public class UserEntity extends BaseEntity
{
    /** The property userId is the generated user id for user */
    @Column(name="user_id", unique=true, nullable=false, length=36)
    private String userId;

    /** The property username is the generated user name for user */
    @Column(name="username", unique=false, nullable=false, length=50)
    private String username;

    /** The property password is the generated password for user */
    @Column(name="password", unique=false, nullable=false, length=72)
    private String password;

    @Enumerated( EnumType.STRING)
    private Role role;




}
